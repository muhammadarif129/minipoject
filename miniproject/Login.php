<?php 
session_start();
include('server.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <link   rel="stylesheet" href="style2.css">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="description"
      content="Web site created using create-react-app"/>

</head>
<body>
    <form class="box" action="index.php" method="POST"> 
    <?php if(isset($_SESSION['error'])) : ?>
      <div class="error">
         <h3>
            <?php
            echo $_SESSION['error'];
            unset($_SESSION['error']);
            ?>
         </h3>
      </div>
    <?php endif?>
    
    <div class="header">
        <h1>Login</h1>
        </div>
    <div class="input-group">
       <input type="text" name="username" placeholder="Username">
    </div>
    <div class="input-group">
       <input type="password" name="password" placeholder="Password">
    </div>
    <div class="input-group">
       <input type="submit" name="" value="Login">
    </div>
       <p class="t">Not yet a member? <a href="registerr.php">Sign Up</a></p>
    </form>
</body>
</html>