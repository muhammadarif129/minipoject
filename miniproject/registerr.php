<?php include('server.php');
 session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Page</title>
    <link   rel="stylesheet" href="style3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="description"
      content="Web site created using create-react-app"/>
</head>
<body>
       <form  class="box" action="register_db.php" method="post">
         <?php include('errors.php') ?> 
       <h1>Register</h1>
            <input type="text" name="username" placeholder="Username" required>
            <input type="text" name="email" placeholder="Email" required>
            <input type="password" name="password_1" placeholder="Password">
            <input type="password" name="password_2" placeholder="Confirm Password">
        <div class="input-group">
           <input type="submit" name="reg_user" value="Register">
        </div>
        <p class="t">Already a member? <a href="Login.php">Sign In</a></p>
        
    </form>
</body>
</html>

